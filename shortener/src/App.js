import logo from './logo.svg';
import './App.css';
import InputShortener from './input-shortener';
import LinkResult from './link-result';
import {useState} from 'react'

function App() {
  const [inputValue, setInputValue] = useState("");
  return (
    <div className="container">
      <InputShortener setInputValue={setInputValue}/>
      <LinkResult  inputValue={inputValue}/>
    </div>
  );
}

export default App;
